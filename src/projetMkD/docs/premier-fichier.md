# Création du premier fichier dans le projet 


***
# Création du répertoire 
***

A partir de la liste des fichiers du projet, il suffit de cliquer sur ajouter et sélectionner un répertoire : 

![Screenshot](img/creer-rep1.png)

Ensuite, dans la fenêtre de dialogue qui s'affiche, il suffit de nomme rle répertoire et de créer un commit : 

![Screenshot](img/creer-rep-commit.png)

***
# Création du fichier 
***
Dans ce répertoire, on va créer un fichier de type markdown : 

![Screenshot](img/creer-fichier.png)

De la même manière, l'enregistrement sera un "commit" :

![Screenshot](img/creer-fichier-2.png)

***
# Vue des commits 
***
On peut retrouver les ajouts que l'on a fait au projet via les commits : 

![Vue de la liste des commits](img/vue-commit.png)

Ou sous la forme d'un graphe : 

![Vue sous forme de graphe](img/vue-graph.png)